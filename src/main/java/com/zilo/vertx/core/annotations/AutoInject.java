package com.zilo.vertx.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@SuppressWarnings("DanglingJavadoc")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
/**
 * This interfaces creates annotation [AutoInject].
 * it is used for one purpose - to signalize for [Launcher]
 * which fields need to be dynamically instantiated and assigned
 * during application startup process. It doesn't accept parameters.
 */
public @interface AutoInject {
}
