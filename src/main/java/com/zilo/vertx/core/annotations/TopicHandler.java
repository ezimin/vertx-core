package com.zilo.vertx.core.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@SuppressWarnings("DanglingJavadoc")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
/**
 * This interfaces creates annotation [Endpoint].
 * it is used for one purpose - to signalize for [Launcher]
 * which fields methods to be dynamically considered as API Endpoints.
 */
public @interface TopicHandler {
    /**
     * Topic name.
     */
    String Topic() default "";
}
