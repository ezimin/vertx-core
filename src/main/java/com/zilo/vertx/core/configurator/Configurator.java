package com.zilo.vertx.core.configurator;

import com.zilo.vertx.core.constants.CommonConstants;
import com.zilo.vertx.core.dto.Components;
import com.zilo.vertx.core.launcher.ComponentBuilder;
import com.zilo.vertx.core.launcher.Launcher;
import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

@SuppressWarnings("PlaceholderCountMatchesArgumentCount")
@Setter
@NoArgsConstructor
public class Configurator extends AbstractVerticle implements Components {

    private static final Logger log = LogManager
        .getLogger(Configurator.class);

    @Getter
    private String deploymentId;

    @Setter
    private static JsonObject configuration;

    private Vertx      vertx = Launcher.getVertx();
    private JsonObject customConfig;

    public Configurator(JsonObject configuration) {
        this.customConfig = configuration;
    }

    /**
     * Main configuration run method which returns unresolved Future object.
     * It will try to reach all possible resources containing configuration
     * and load them, overwriting if necessary. The order is the following
     * (and latest overwrites earliest): DEFAULT, JSON file, YAML file, K8s.
     *
     * @return voided future to be resolved
     */
    public Future<JsonObject> init() {
        return ConfigRetriever.getConfigAsFuture(createConfigRetriever());
    }

    /**
     * This method is kinda common stuff to setup stores and sources for any config.
     * The main usage of this method for Future and Resolved configs only.
     *
     * @return ConfigRetriever setup stores
     */
    private ConfigRetriever createConfigRetriever() {
        ConfigRetrieverOptions configRetrieverOptions = new ConfigRetrieverOptions();

        log.debug("Loading configuration from JSON: application.json");
        ConfigStoreOptions json = new ConfigStoreOptions()
            .setType(CommonConstants.FILE)
            .setFormat(CommonConstants.JSON)
            .setOptional(true)
            .setConfig(new JsonObject().put(CommonConstants.PATH, "application.json"));
        configRetrieverOptions.addStore(json);

        log.debug("Loading configuration from YAML: application.yaml");
        ConfigStoreOptions yaml = new ConfigStoreOptions()
            .setType(CommonConstants.FILE)
            .setFormat(CommonConstants.YAML)
            .setOptional(true)
            .setConfig(new JsonObject().put(CommonConstants.PATH, "application.yaml"));
        configRetrieverOptions.addStore(yaml);

        log.debug("Using environment variables for the configuration");
        ConfigStoreOptions env = new ConfigStoreOptions().setType("env");
        configRetrieverOptions.addStore(env);

        return ConfigRetriever.create(vertx, configRetrieverOptions);
    }

    private static JsonObject getAll() {
        return configuration;
    }

    /**
     * This is another nice stuff how to get configuration values.
     * You can get an old way or you can get a new one and try to get something like:
     * config("vertx.kafka.consumer.topics) which returns you whatever is sitting under that
     * parameter or null.
     *
     * @param configParam a parameter you'd like to read like [vertx.kafka.consumer.topics]
     * @param <T>         a type of returning object
     * @return actual object of type T
     */
    public static <T> T get(String configParam) {
        String[] params = configParam.split("\\.");
        return readRecursiveValue(configuration, params, 0);
    }

    /**
     * The same as above but the source for it is different - it's a custom config.
     *
     * @param configParam a parameter you'd like to read like [vertx.kafka.consumer.topics]
     * @param <T>         a type of returning object
     * @return actual object of type T
     */
    public <T> T getFromCustomConfig(String configParam) {
        String[] params = configParam.split("\\.");
        return readRecursiveValue(customConfig, params, 0);
    }

    /**
     * Tries to recursively load the requesting value of parameter.
     * if parameter is not set or not found - returns null.
     * May return any type of values, thus need direct type casting.
     *
     * @param currentConfig configuration of type JsonObject
     * @param params        all params split and stored as an array
     * @param index         current index of the word in parameter statement
     * @param <T>           type for casting
     * @return final value WITHOUT casting
     */
    private static <T> T readRecursiveValue(JsonObject currentConfig, String[] params, int index) {
        // make default initialization of return value
        T returnVal = null;

        // check if we need to return JsonObject
        if (params.length == index) {
            //noinspection unchecked
            returnVal = (T) currentConfig;
        } else if (currentConfig.containsKey(params[index])) {
            if (currentConfig.getValue(params[index]) instanceof JsonObject) {
                JsonObject newConfig = currentConfig.getJsonObject(params[index]);
                returnVal = readRecursiveValue(newConfig, params, index + 1);
            } else {
                //noinspection unchecked
                returnVal = (T) currentConfig.getValue(params[index]);
            }
        }
        // eventually return something... anything... whatever
        return returnVal;
    }

    /**
     * This method is responsible for pulling down any dynamic configuration.
     * Configuration is stored in Kubernetes ConfigMaps and Kubernetes Secrets.
     * In case of any errors application will not be terminated, but appropriate
     * [error] or [warning] will be thrown.
     *
     * @param mainClass Application Main class
     */
    public void loadDynamicConfiguration(Class<?> mainClass) {

        ComponentBuilder componentBuilder = new ComponentBuilder(vertx);
        boolean dynamicConfigEnabled =
            (boolean) Optional.ofNullable(Configurator.get("vertx.configuration.dynamic-configuration"))
                .orElse(false);

        if (!dynamicConfigEnabled) {
            log.debug("Dynamic configuration is not defined");
            componentBuilder.prepareAnnotatedComponents(mainClass);
            return;
        }

        String k8sConfMapNamespace =
            Optional.ofNullable(Configurator.get("vertx.configuration.kubernetes.configmap.namespace"))
                .orElse("").toString();

        String k8sConfMapName =
            Optional.ofNullable(Configurator.get("vertx.configuration.kubernetes.configmap.name"))
                .orElse("").toString();

        String k8sSecretNamespace =
            Optional.ofNullable(Configurator.get("vertx.configuration.kubernetes.configmap.namespace"))
                .orElse("").toString();

        String k8sSecretName =
            Optional.ofNullable(Configurator.get("vertx.configuration.kubernetes.configmap.name"))
                .orElse("").toString();

        ConfigRetrieverOptions configRetrieverOptions = new ConfigRetrieverOptions();

        if (!k8sConfMapNamespace.isEmpty() && !k8sConfMapName.isEmpty()) {
            configRetrieverOptions.addStore(new ConfigStoreOptions().setType(CommonConstants.K8S_CONFIGMAP).setConfig(
                new JsonObject().put(CommonConstants.K8S_NAMESPACE, k8sConfMapNamespace)
                    .put(CommonConstants.K8S_NAME, k8sConfMapName)));
        } else {
            log.warn("Dynamic configuration is enabled but missing data for connection");
        }

        if (!k8sSecretNamespace.isEmpty() && !k8sSecretName.isEmpty()) {
            configRetrieverOptions.addStore(new ConfigStoreOptions().setType(CommonConstants.K8S_CONFIGMAP).setConfig(
                new JsonObject().put(CommonConstants.K8S_NAMESPACE, k8sSecretNamespace)
                    .put(CommonConstants.K8S_NAME, k8sSecretName)
                    .put(CommonConstants.K8S_SECRET, true)));
        } else {
            log.warn("Dynamic secret is enabled but missing data for connection");
        }

        ConfigRetriever.create(vertx, configRetrieverOptions).getConfig(configStore -> {
            if (configStore.succeeded()) {
                Configurator.setConfiguration(Configurator.getAll().mergeIn(configStore.result(), true));
                componentBuilder.prepareAnnotatedComponents(mainClass);
            } else {
                log.error("K8s config failed. Response: {}", configStore.cause());
            }
        });
    }
}
