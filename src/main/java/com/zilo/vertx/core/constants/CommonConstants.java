package com.zilo.vertx.core.constants;

import io.vertx.core.json.JsonObject;

@SuppressWarnings("WeakerAccess")
public class CommonConstants {
    // General Section
    public static final String VERTX       = "vertx";
    public static final String PATH        = "path";
    public static final String FILE        = "file";
    public static final String YAML        = "yaml";
    public static final String JSON        = "json";
    public static final String PORT        = "port";
    public static final String HOST        = "host";
    public static final String CORS        = "cors";
    public static final String USE_SSL     = "vertx.server.ssl";
    public static final String JKS_PATH    = "vertx.server.authorization.jwt.jks_path";
    public static final String JKS_PASS    = "vertx.server.authorization.jwt.jks_pass";
    public static final String SASL_SSL    = "SASL_SSL";

    public static final String CONFIGURATION  = "configuration";
    public static final String DYNAMIC_CONFIG = "dynamic-configuration";
    public static final String K8S            = "kubernetes";
    public static final String K8S_CONFIGMAP  = "configmap";
    public static final String K8S_NAMESPACE  = "namespace";
    public static final String K8S_NAME       = "name";
    public static final String K8S_SECRET     = "secret";

    public static final  String SERVER                         = "server";
    public static final  String SERVER_RESPONSE_STATUS_MESSAGE = "result";
    private static final String SERVER_RESPONSE_STATUS_CODE    = "status";
    private static final String SERVER_RESPONSE_BODY           = "message";
    public static final  String COUCHBASE_ACCESS_ERROR         = "ACCESS_ERROR";

    public static final String HTTP200  = new JsonObject("{\""
                                                         + SERVER_RESPONSE_STATUS_CODE + "\": 200, \""
                                                         + SERVER_RESPONSE_BODY + "\":\"OK\"}").toString();
    public static final String HTTP400  = new JsonObject("{\""
                                                         + SERVER_RESPONSE_STATUS_CODE + "\": 400, \""
                                                         + SERVER_RESPONSE_BODY + "\":\"Bad Request\"}").toString();
    public static final String HTTP401  = new JsonObject("{\""
                                                         + SERVER_RESPONSE_STATUS_CODE + "\": 401, \""
                                                         + SERVER_RESPONSE_BODY + "\":\"Not Authorized\"}").toString();
    public static final String HTTP403  = new JsonObject("{\""
                                                         + SERVER_RESPONSE_STATUS_CODE + "\": 403, \""
                                                         + SERVER_RESPONSE_BODY + "\":\"Forbidden\"}").toString();
    public static final String HTTP404  = new JsonObject("{\""
                                                         + SERVER_RESPONSE_STATUS_CODE + "\": 404, \""
                                                         + SERVER_RESPONSE_BODY + "\":\"Document not found\"}").toString();
    public static final String HTTP409  = new JsonObject("{\""
                                                         + SERVER_RESPONSE_STATUS_CODE + "\": 409, \""
                                                         + SERVER_RESPONSE_BODY + "\":\"Document already Exists\"}").toString();

    public static final String ORGANIZATION_ID = "organizationId";
    public static final String USER_ID         = "userId";
    public static final String API_URL         = "apiURL";
    public static final String EMAIL_ADDRESS   = "emailAddress";


    public static final String RS256             = "RS256";
    public static final String KEYS              = "keys";
    public static final String CLIENTID          = "clientid";


    public static final String UTF_8 = "UTF-8";

    // HTTP Headers Section
    public static final String HEADER_ACCEPT_ENCODING                  = "Accept-Encoding";
    public static final String HEADER_APPLICATION_JSON                 = "application/json";
    public static final String HEADER_CONTENT_TYPE                     = "Content-Type";
    public static final String HEADER_ACCEPT                           = "Accept";
    public static final String HEADER_ACCESS_CONTROL_ALLOW_ORIGIN      = "Access-Control-Allow-Origin";
    public static final String HEADER_ACCESS_CONTROL_ALLOW_HEADERS     = "Access-Control-Allow-Headers";
    public static final String HEADER_ACCESS_CONTROL_ALLOW_METHODS     = "Access-Control-Allow-Methods";
    public static final String HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    public static final String HEADER_ACCESS_CONTROL_EXPOSE_HEADERS    = "Access-Control-Expose-Headers";
    public static final String HEADER_AUTHORIZATION                    = "Authorization";
    public static final String HEADER_X_ZILO_CLIENT_ID                 = "X-ZILO-Client-Id";
    public static final String HEADER_X_ZILO_CLIENT_VERSION            = "X-ZILO-Client-Version";
    public static final String HEADER_CACHE_CONTROL                    = "Cache-control";
    public static final String HEADER_NO_CACHE                         = "no-cache";
    public static final String HEADER_PRAGMA                           = "Pragma";
    public static final String HEADER_NO_STORE                         = "no-store";

    // HTTP Auth Section
    public static final String AUTHORIZATION     = "authorization";
    public static final String ENGAGE_URL_REGEX  = "engage_url_regex";
    public static final String PERMISSIONS_PATH  = "permissions_path";
    public static final String PERMISSIONS_QUERY = "permissions_query";
    public static final String PRIVATE_KEY       = "private_key";
    public static final String PUBLIC_KEY        = "public_key";
    public static final String OVERRIDE          = "override";

    // Kafka Section
    private static final String KAFKA                      = "kafka";
    private static final String KAFKA_CONSUMER             = "consumer";
    public static final  String KAFKA_SECURITY             = "security";
    public static final  String KAFKA_PRODUCER             = "producer";
    public static final  String KAFKA_ADMIN                = "admin";
    private static final String KAFKA_BOOTSTRAP_SERVERS    = "bootstrap-servers";
    public static final  String CONSUMER_BOOTSTRAP_SERVERS =
        VERTX + "." + KAFKA + "." + KAFKA_CONSUMER + "." + KAFKA_BOOTSTRAP_SERVERS;
    public static final  String KAFKA_GROUP_ID             = "group-id";
    private static final String KAFKA_TOPICS               = "topics";
    public static final  String CONSUMER_TOPICS            =
        VERTX + "." + KAFKA + "." + KAFKA_CONSUMER + "." + KAFKA_TOPICS;
    public static final  String KAFKA_ACKS                 = "acks";
    public static final  String KAFKA_USERNAME             = "sasl-username";
    public static final  String KAFKA_PASSWORD             = "sasl-password"; //NOSONAR

    public static final String KAFKA_SECURITY_PROTOCOL      = "security-protocol";
    public static final String KAFKA_SASL_JAAS_CONFIG       = "sasl-jaas-config";
    public static final String KAFKA_SASL_MECHANISM         = "sasl-mechanism";
    public static final String KAFKA_SSL_PROTOCOL           = "ssl-protocol";
    public static final String KAFKA_SSL_ENABLED_PROTOCOLS  = "ssl-enabled-protocols";
    public static final String KAFKA_SSL_ENDPOINT_IDENT_ALG = "ssl-endpoint-identification-algorithm";

    public static final String KAFKA_KEY_DESERIALIZER   = "key-deserializer";
    public static final String KAFKA_VALUE_DESERIALIZER = "value-deserializer";
    public static final String KAFKA_KEY_SERIALIZER     = "key-serializer";
    public static final String KAFKA_VALUE_SERIALIZER   = "value-serializer";

    public static final String KAFKA_AUTO_OFFSET_RESET  = "auto-offset-reset";
    public static final String KAFKA_ENABLE_AUTO_COMMIT = "enable-auto-commit";

    // Shell Section
    public static final String SHELL          = "shell";
    public static final String SHELL_API_URL  = "api_url";
    public static final String SHELL_CLIENTID = "clientid";

    // DataBase Section
    public static final String COUCHBASE         = "couchbase";
    public static final String BUCKET            = "bucket";
    public static final String STALE             = "stale";
    public static final String USER              = "user";
    public static final String PASSWORD          = "password";  //NOSONAR
    public static final String SSL_KEYSTORE_FILE = "ssl_keystore_file";
    public static final String SSL_KEYSTORE_PASS = "ssl_keystore_password";

    // Admin Constants
    public static final String COMMANDS_PATH       = "vertx.admin.commands.";
    public static final String COMMAND_KEY         = "command";
    public static final String PARAMS_KEY          = "params";
    public static final String EXECUTE_METHOD_NAME = "execute";
    public static final String ADMIN_ENABLE_PATH   = "vertx.admin.server.enable";
    public static final String ADMIN_SERVER_CONFIG = "vertx.admin.server";
    public static final String ADMIN_SERVER_JWTKEY = "vertx.admin.server.jwtKey";

    private CommonConstants() {
    }
}
