package com.zilo.vertx.core.constants;

import io.vertx.core.json.JsonObject;

public class HttpConstants {
    public static final  String VERTX                                   = "vertx";
    public static final  String PORT                                    = "port";
    public static final  String HOST                                    = "host";
    public static final  String CORS                                    = "cors";
    public static final  String SSL                                     = "ssl";
    public static final  String USE_SSL                                 = "ssl";
    public static final  String SERVER                                  = "vertx.server";
    public static final  String SERVER_RESPONSE_STATUS_MESSAGE          = "result";
    private static final String SERVER_RESPONSE_STATUS_CODE             = "status";
    private static final String SERVER_RESPONSE_BODY                    = "message";
    // HTTP Headers Section
    public static final  String HEADER_ACCEPT_ENCODING                  = "Accept-Encoding";
    public static final  String HEADER_APPLICATION_JSON                 = "application/json";
    public static final  String HEADER_CONTENT_TYPE                     = "Content-Type";
    public static final  String HEADER_ACCEPT                           = "Accept";
    public static final  String HEADER_ACCESS_CONTROL_ALLOW_ORIGIN      = "Access-Control-Allow-Origin";
    public static final  String HEADER_ACCESS_CONTROL_ALLOW_HEADERS     = "Access-Control-Allow-Headers";
    public static final  String HEADER_ACCESS_CONTROL_ALLOW_METHODS     = "Access-Control-Allow-Methods";
    public static final  String HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    public static final  String HEADER_ACCESS_CONTROL_EXPOSE_HEADERS    = "Access-Control-Expose-Headers";
    public static final  String HEADER_AUTHORIZATION                    = "Authorization";
    public static final  String HEADER_X_ZILO_CLIENT_ID                 = "X-ZILO-Client-Id";
    public static final  String HEADER_X_ZILO_CLIENT_VERSION            = "X-ZILO-Client-Version";
    public static final  String HEADER_CACHE_CONTROL                    = "Cache-control";
    public static final  String HEADER_NO_CACHE                         = "no-cache";
    public static final  String HEADER_PRAGMA                           = "Pragma";
    public static final  String HEADER_NO_STORE                         = "no-store";
    // HTTP Auth Section
    public static final  String OAUTH2_ENABLED                          = "authentication.oauth2.enabled";
    public static final  String OAUTH2_CLIENT_ID                        = "authentication.client-id";
    public static final  String OAUTH2_CLIENT_SECRET                    = "authentication.client-secret";
    public static final  String OAUTH2_LOGIN                            = "authentication.oauth2.login";
    public static final  String OAUTH2_LOGOUT                           = "authentication.oauth2.logout";
    public static final  String OAUTH2_CALLBACK                         = "authentication.oauth2.callback";
    public static final  String OAUTH2_HOME                             = "authentication.oauth2.home";
    public static final  String OAUTH2_DEFAULT_PROVIDER                 = "authentication.oauth2.default-provider";
    public static final  String OAUTH2_SUPPORTED_PROVIDERS              = "authentication.oauth2.supported-providers";
    public static final  String OAUTH2_AUTH_PROVIDER                    = "auth-provider";
    public static final  String KEY_PATH                                = "authorization.jwt.keypath";
    public static final  String CERT_PATH                               = "authorization.jwt.certpath";
    public static final  String AUTHZ_PATHS                             = "authorization.path";
    public static final  String AUTHORIZATION                           = "authorization";

    public static final String HTTP200 = new JsonObject("{\""
                                                        + SERVER_RESPONSE_STATUS_CODE + "\": 200, \""
                                                        + SERVER_RESPONSE_BODY + "\":\"OK\"}").toString();
    public static final String HTTP400 = new JsonObject("{\""
                                                        + SERVER_RESPONSE_STATUS_CODE + "\": 400, \""
                                                        + SERVER_RESPONSE_BODY + "\":\"Bad Request\"}").toString();
    public static final String HTTP401 = new JsonObject("{\""
                                                        + SERVER_RESPONSE_STATUS_CODE + "\": 401, \""
                                                        + SERVER_RESPONSE_BODY + "\":\"Not Authorized\"}").toString();
    public static final String HTTP403 = new JsonObject("{\""
                                                        + SERVER_RESPONSE_STATUS_CODE + "\": 403, \""
                                                        + SERVER_RESPONSE_BODY + "\":\"Forbidden\"}").toString();
    public static final String HTTP404 = new JsonObject("{\""
                                                        + SERVER_RESPONSE_STATUS_CODE + "\": 404, \""
                                                        + SERVER_RESPONSE_BODY + "\":\"Document not found\"}").toString();
    public static final String HTTP409 = new JsonObject("{\""
                                                        + SERVER_RESPONSE_STATUS_CODE + "\": 409, \""
                                                        + SERVER_RESPONSE_BODY + "\":\"Document already Exists\"}").toString();


    private HttpConstants() {}
}
