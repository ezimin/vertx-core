package com.zilo.vertx.core.dto;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ComponentInstance {
    private String        id;
    private String        deploymentId;
    private ComponentType componentType;
    private Components    component;
    private String        configLink;
    private JsonObject    configuration;
    private Class<?>      controllerClass;
    private Controllers   controller;
    private List<String>  dependants = new ArrayList<>();
    private Boolean       componentReady;
    private Vertx         vertx;
}
