package com.zilo.vertx.core.dto;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public enum ComponentType {
    // Kafka related components
    KAFKA_LISTENER("initKafkaListener", "vertx.kafka.listener", "KafkaListener"),
    KAFKA_SENDER("initKafkaProducer", "vertx.kafka.producer", "KafkaProducer"),

    // DB related components
    COUCHBASE("initCouchBase", "vertx.couchbase", "CouchbaseConnector"),
    CASSANDRA("initCassandra", "vertx.cassandra", "CassandraConnector"),
    MONGODB("initMongoDB", "vertx.mongodb", "MongoDbConnector"),
    POSTGRES("initPostgres", "vertx.postgres", "PostgresConnector"),
    MARIADB("initMariaDB", "vertx.mariadb", "MariaDbConnector"),
    MYSQL("initMysql", "vertx.mysql", "MySqlConnector"),
    MSSQL("initMssql", "vertx.mssql", "MsSqlConnector"),

    // HTTP Related components
    HTTP_SERVER("initHttpServer", "vertx.server", "HttpServer"),
    HTTP_CLIENT("initHttpClient", "vertx.httpclient", "HttpClient");

    private static final Map<String, ComponentType> BY_LABEL = new HashMap<>();

    static {
        for (ComponentType e : values()) {
            BY_LABEL.put(e.typeClass, e);
        }
    }

    @Getter
    private final String initMethod;

    @Getter
    private final String defaultConfig;

    @Getter
    private final String typeClass;

    ComponentType(String initMethod, String defaultConfig, String typeClass) {
        this.initMethod    = initMethod;
        this.defaultConfig = defaultConfig;
        this.typeClass     = typeClass;
    }

    public static ComponentType valueOfLabel(String label) {
        return BY_LABEL.get(label);
    }
}
