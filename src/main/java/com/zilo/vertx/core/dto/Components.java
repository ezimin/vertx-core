package com.zilo.vertx.core.dto;

import io.vertx.core.Future;

@SuppressWarnings("all")
public interface Components {
    /**
     * This method starts any component of type <Components>
     */
    Future<?> init();
}
