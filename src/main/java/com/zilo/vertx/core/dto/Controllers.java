package com.zilo.vertx.core.dto;

/**
 * This is general interfaces to define the type class for all controllers.
 */
public interface Controllers {}
