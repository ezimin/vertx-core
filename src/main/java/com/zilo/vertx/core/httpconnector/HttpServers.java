package com.zilo.vertx.core.httpconnector;

import com.zilo.vertx.core.dto.Components;
import io.vertx.core.http.HttpMethod;

public interface HttpServers extends Components {

    /**
     * Simple overloaded wrapper for main private method.
     * It is used for setting declareController in simpler way, providing some default headers.
     *
     * @param httpMethod List of methods to use for registering controllers
     * @param path       actual path which needs to be added to a router handler
     * @param callBack   method name from [controllers] object to serve the response
     */
    void endpoint(HttpMethod httpMethod, String path, String callBack);
}
