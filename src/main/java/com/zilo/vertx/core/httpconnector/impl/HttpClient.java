package com.zilo.vertx.core.httpconnector.impl;

import com.zilo.vertx.core.constants.CommonConstants;
import com.zilo.vertx.core.dto.ComponentInstance;
import com.zilo.vertx.core.dto.Components;
import com.zilo.vertx.core.launcher.Launcher;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.WebClientSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpClient extends AbstractVerticle implements Components {

    private String componentId;

    private Vertx vertx;

    private WebClientSession httpClient;

    private static final Logger log = LogManager.getLogger(HttpClient.class);

    public HttpClient(ComponentInstance componentInstance) {
        this.componentId = componentInstance.getId();
        this.vertx       = componentInstance.getVertx();
        WebClientOptions options = new WebClientOptions()
            .setUserAgent("Vertx-Web-Client/1.0")
            .setKeepAlive(false);
        WebClient simpleClient = WebClient.create(vertx, options);
        httpClient = WebClientSession.create(simpleClient);
        httpClient.addHeader(CommonConstants.HEADER_CONTENT_TYPE, CommonConstants.HEADER_APPLICATION_JSON);
        httpClient.addHeader(CommonConstants.HEADER_CACHE_CONTROL, CommonConstants.HEADER_NO_CACHE + ", " + CommonConstants.HEADER_NO_STORE);
        httpClient.addHeader(CommonConstants.HEADER_PRAGMA, CommonConstants.HEADER_NO_CACHE);
    }

    @Override
    public Future<Void> init() {
        Promise<Void> promise = Promise.promise();
        vertx.deployVerticle(this, response -> {
            if (response.succeeded()) {
                String deploymentId = response.result();

                ComponentInstance componentInstance = Launcher.getComponent(this.componentId);
                componentInstance.setId(this.componentId);
                componentInstance.setDeploymentId(deploymentId);
                componentInstance.setController(null);
                componentInstance.setComponentReady(true);
                promise.complete();

                log.debug("[{}] HTTP client verticle deployment ID is: {}", componentInstance.getId(), deploymentId);
            }
        });
        return promise.future();
    }

    public WebClientSession getHttpClient() {
        return httpClient;
    }
}
