package com.zilo.vertx.core.httpconnector.impl;

import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.constants.HttpConstants;
import com.zilo.vertx.core.dto.ComponentInstance;
import com.zilo.vertx.core.dto.Controllers;
import com.zilo.vertx.core.httpconnector.HttpServers;
import com.zilo.vertx.core.httpconnector.oauth2.grants.OAuth2GrantType;
import com.zilo.vertx.core.httpconnector.oauth2.providers.OAuth2AuthProvider;
import com.zilo.vertx.core.launcher.Launcher;
import com.zilo.vertx.core.utils.Utils;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.Cookie;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("PlaceholderCountMatchesArgumentCount")
public class HttpServer extends AbstractVerticle implements HttpServers {

    @Getter
    private Class<?> controllerClass;

    @Getter
    private OAuth2Auth oauth2;

    private io.vertx.core.http.HttpServer httpServer;
    private Configurator                  configurator;
    private Router                        router;
    private JsonObject                    httpConfig;
    private String                        componentId;
    private Controllers                   controller;
    private JWTAuth                       jwtAuthProvider;
    private OAuth2AuthProvider            authProvider;

    private static final Logger log = LogManager.getLogger(HttpServer.class);

    /**
     * Default constructor to create HttpServer instance with given configuration.
     *
     * @param componentInstance [Component] for Abstract Factory, needed to be instantiated
     *                          through this procedure. It is a self-contained object with DTO
     *                          under the [dto] package. Inherits [Components] interface and managed
     *                          by [ComponentLoader] and [Launcher]. Allowed to be reconfigured,
     *                          re-deployed, stopped, destroyed, etc.
     * @throws ExceptionInInitializerError in case HttpServer cannot be instantiated
     */
    public HttpServer(ComponentInstance componentInstance) throws ExceptionInInitializerError {
        this.componentId     = componentInstance.getId();
        this.vertx           = componentInstance.getVertx();
        this.httpConfig      = componentInstance.getConfiguration();
        this.controllerClass = componentInstance.getControllerClass();
        this.router          = Router.router(this.vertx);
    }

    /**
     * This method initiate the httpServer's Promise/Future with given configuration.
     *
     * @return Future to resolve by [ComponentLoader]
     */
    public Future<Void> init() {
        // custom promise to resolve
        Promise<Void> promise = Promise.promise();

        // validate the minimal server configuration and create custom Configurator inside
        Optional.ofNullable(validateConfiguration(httpConfig))
            .orElseThrow(() -> new ExceptionInInitializerError("HttpServer configuration is invalid."));

        // setup common system resources
        Integer serverPort = configurator.getFromCustomConfig(HttpConstants.PORT);
        String  serverHost = configurator.getFromCustomConfig(HttpConstants.HOST);

        // create basic handler to manage response body
        router.route().handler(BodyHandler.create());

        // create oauth2 routes for authentication purposes
        if (configurator.getFromCustomConfig(HttpConstants.OAUTH2_ENABLED)) {
            // route for login operations
            router.route(configurator.getFromCustomConfig(HttpConstants.OAUTH2_LOGIN)).handler(this::authentication);
            // route for logout operations
            router.route(configurator.getFromCustomConfig(HttpConstants.OAUTH2_LOGOUT)).handler(this::authLogout);
            // route for callback operations for using with Auth Code Flow of OAuth 2.0
            router.route(configurator.getFromCustomConfig(HttpConstants.OAUTH2_CALLBACK)).handler(this::authCallback);
            // we need this JWT Auth Provider only once for Object life-cycle to validate JWT as well as generate them
            this.jwtAuthProvider = createJwtAuthProvider();
        }

        // create server options to fill in with ssl, headers, auth, etc.
        HttpServerOptions httpOpts = new HttpServerOptions();
        // check whether or not we need to enable SSL. These variables are also environment.
        // For debugging purposes you can generate a self-signed certificate
        // export vertx='{"server":{"authentication":{"client-id":"<>","client-secret":"<>"},
        // "authorization":{"jwt":{"keypath":"<private_key_pk8.pem>","certpath":"<private_cert.pem>"}}}}}}'
        // #############################################################################################################
        //                                  FOR DEVELOPMENT PURPOSES ONLY!!!
        // How-to create self-signed certificate:
        // openssl req -x509 -newkey rsa:4096 -keyout private_key.pem -out private_cert.pem -days 365 -subj '/CN=localhost'
        // openssl pkcs8 -topk8 -inform PEM -outform PEM -in private_key.pem -out private_key_pk8.pem -nocrypt
        // openssl rsa -in private_key.pem -outform PEM -pubout -out public_key.pem
        // #############################################################################################################
        if ((boolean) Optional.ofNullable(configurator.getFromCustomConfig(HttpConstants.USE_SSL)).orElse(false)) {
            String keyPath = Optional.ofNullable(configurator.getFromCustomConfig(HttpConstants.KEY_PATH))
                .orElseThrow(() -> new ExceptionInInitializerError("HttpsServer configuration is missing parameter ["
                                                                   + HttpConstants.KEY_PATH + "]")).toString();
            String certpath = Optional.ofNullable(configurator.getFromCustomConfig(HttpConstants.CERT_PATH))
                .orElseThrow(() -> new ExceptionInInitializerError("HttpsServer configuration is missing parameter ["
                                                                   + HttpConstants.CERT_PATH + "]")).toString();
            PemKeyCertOptions pemKeyCertOptions = new PemKeyCertOptions().setKeyPath(keyPath).setCertPath(certpath);
            httpOpts.setPemKeyCertOptions(pemKeyCertOptions);
            httpOpts.setSsl(true);
        }

        try {
            this.httpServer = vertx.createHttpServer(httpOpts);
        } catch (Exception e) {
            log.error("HTTP(-s) server has failed during creation. Stacktrace:\n {}", e);
            promise.fail("HTTP(-s) server has failed during creation.");
        }

        // after server started by itself it is necessary to do other things - deploy verticles (in case
        // anybody wants to work with Event Bus and have many verticles), initialize controller and
        // start it, making dependency injection into it, and push initialized components, including
        // server itself, into an Abstract Factory, making them accessible through [Launcher].
        startServer(promise, serverPort, serverHost);

        return promise.future();
    }

    /**
     * Overloaded wrapper/helper for method responsible to call actual business logic for the endpoint.
     * It is a public one aiming to validate, hide, overload, etc.
     *
     * @param httpMethod List of methods to use for registering controllers
     * @param path       actual path which needs to be added to a router handler
     * @param callBack   method name from [controllers] object to serve the response
     */
    public void endpoint(HttpMethod httpMethod, String path, String callBack) {
        Map<String, String> headers = new HashMap<>();
        // setup extra headers, like CORS, response type and GZIP
        headers.put(HttpConstants.HEADER_ACCEPT_ENCODING, "gzip, deflate, br");
        //headers.put(HttpConstants.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
        headers.put(HttpConstants.HEADER_PRAGMA, HttpConstants.HEADER_NO_CACHE);
        headers.put(HttpConstants.HEADER_ACCESS_CONTROL_ALLOW_ORIGIN,
            configurator.getFromCustomConfig(HttpConstants.CORS));
        headers.put(HttpConstants.HEADER_CACHE_CONTROL,
            HttpConstants.HEADER_NO_CACHE + ", " + HttpConstants.HEADER_NO_STORE);

        // register normal route
        controllerImpl(httpMethod, path, callBack, headers);
    }

    /**
     * This method checks for minimal configuration parameters required for HttpServer start working.
     * If the configuration is correct - server returns custom configuration, which will be isolated
     * from main config. That's is done for the purpose if developer needs to create multiple servers
     * listening different ports, for example, or serving different requests.
     *
     * @param httpCustomConfig input configuration to make isolated. If there are any missing parameters,
     *                         it tries to substitute them from default section of [server] from config
     * @return isolated copy of configuration or null if there are any missing params
     * @throws ExceptionInInitializerError only when method input parameter is not set and null
     */
    private JsonObject validateConfiguration(JsonObject httpCustomConfig) throws ExceptionInInitializerError {

        httpCustomConfig = Optional.ofNullable(httpCustomConfig)
            .orElseThrow(() -> new ExceptionInInitializerError("HttpServer configuration is invalid."));
        // flag to signalize if the configuration valid or not
        boolean valid = true;

        // final configuration which will be used to initialize current Kafka instance
        // we grab all configuration from common section and then simply overwrite
        // existing values with new ones which we get as constructor parameter
        JsonObject configuration = ((JsonObject) Configurator.get(HttpConstants.SERVER)).copy()
            .mergeIn(httpCustomConfig, true);

        // this is the list of required parameters which should not be set to null or empty
        List<String> parameters = Arrays.asList(HttpConstants.HOST,
            HttpConstants.PORT,
            HttpConstants.SSL,
            HttpConstants.CORS);

        // setup new CUSTOM Configurator object
        this.configurator = new Configurator(configuration);

        // checking all required parameters for null and empty values
        for (String parameter : parameters) {
            if (configurator.getFromCustomConfig(parameter) == null) {
                log.error("Parameter [{}] for HttpServer is absent in your config.", parameter);
                valid = false;
            } else if (configurator.getFromCustomConfig(parameter).toString().isEmpty()) {
                log.error("Parameter [{}] for HttpServer is not set.", parameter);
                valid = false;
            }
        }

        // return valid configuration or null
        return valid ? configuration : null;
    }

    /**
     * Start HttpServer to open TCP socket and listen to connection.
     *
     * @param promise    Promise to mark this step resolved (succeeded/failed)
     * @param serverPort Port number to listen to
     * @param serverHost Host name to limit (may be 0.0.0.0 as well)
     */
    private void startServer(Promise<Void> promise, Integer serverPort, String serverHost) {
        httpServer.requestHandler(router).listen(serverPort, serverHost, asyncResult -> {
            if (asyncResult.succeeded()) {
                // HttpServer is running and we need to deploy its Verticle if anyone would like to use it
                // we also need to setup server security and it can be done inside deploying a Verticle method
                // due to using the same [promise] instance to track the result
                log.info("HTTP server running on host {}:{}", serverHost, serverPort);
                deployServerVerticle(promise);
            } else {
                log.error("Could not start a HTTP server", asyncResult.cause());
                promise.fail(asyncResult.cause());
            }
        });
    }

    /**
     * Main deployment method of the type [Component].
     * After server started by itself it is necessary to do other things - deploy verticles (in case
     * anybody wants to work with Event Bus and have many verticles), initialize controller and
     * start it, making dependency injection into it, and push initialized components, including
     * server itself, into an Abstract Factory, making them accessible through [Launcher].
     *
     * @param promise Promise to mark this step resolved (succeeded/failed)
     */
    private void deployServerVerticle(Promise<Void> promise) {
        vertx.deployVerticle(this, response -> {
            if (response.succeeded()) {
                String deploymentId = response.result();

                // we need to instantiate Controller for Http Server here, because Controller
                // may have its own dependencies. To inject them - we have to have the instance
                // where we will start injecting.
                try {
                    this.controller = (Controllers) controllerClass.getDeclaredConstructor().newInstance();
                } catch (InstantiationException | IllegalAccessException |
                    InvocationTargetException | NoSuchMethodException e) {
                    log.error("Could not create controller {}", e);
                }

                // finalize component itself
                ComponentInstance componentInstance = Launcher.getComponent(this.componentId);
                componentInstance.setDeploymentId(deploymentId);
                componentInstance.setController(this.controller);
                componentInstance.setComponentReady(true);

                // logging result
                log.debug("HTTP server verticle deployment ID is: {}", deploymentId);

                // and completing the promise
                promise.complete();
            }
        });
    }

    /**
     * Mainly this methos is needed for OAuth 2.0 redirections.
     * Since we can't rely on configuration host data and it can be 0.0.0.0, so we need to
     * grab HOST information for redirection purposes from RoutingContext instances.
     *
     * @param routingContext Vertx RoutingContext data, containing everything related to current
     *                       connection including request, response, etc.
     * @return String for the [Protocol][Host][Port]
     */
    private String getHostUrl(RoutingContext routingContext) {
        String protocol = configurator.getFromCustomConfig(HttpConstants.SSL) ? "https://" : "http://";
        return protocol + routingContext.request().host();
    }

    /**
     * This method setup proper OAuth 2.0 Grant Type.
     * Settings is based on cookie (for now), and if it is not set, null, or has the wrong value
     * then it will be chosen default Grant type, which is Authorization Code
     *
     * @param routingContext Vertx RoutingContext data, containing everything related to current
     *                       connection including request, response, etc.
     */
    private void authentication(RoutingContext routingContext) {
        // we need to choose the Authentication Flow from OAuth 2.0. It should be defined
        // in cookies or in headers
        Cookie          cookieOAuth2GrantType = routingContext.getCookie("oauth2-grant-type");
        OAuth2GrantType oAuth2GrantType       = null;
        if (cookieOAuth2GrantType == null || Utils.isNullOrBlank(cookieOAuth2GrantType.getValue())) {
            oAuth2GrantType = OAuth2GrantType.AUTH_CODE;
        } else if (OAuth2GrantType.contains(cookieOAuth2GrantType.getValue().toUpperCase(Locale.ENGLISH))) {
            oAuth2GrantType = OAuth2GrantType.valueOf(cookieOAuth2GrantType.getValue().toUpperCase(Locale.ENGLISH));
        } else {
            oAuth2GrantType = OAuth2GrantType.AUTH_CODE;
        }
        authentication(routingContext, oAuth2GrantType);
    }

    /**
     * Method responsible for initial authentication procedure through OAuth 2.0 with Auth Code Flow.
     * This one is called when unauthenticated user tries to reach protected source. List of protected
     * sources are defined under [vertx.server.authorization.path] section of the configuration file
     * and listed there as the simple array of regexes.
     * Since there are many auth providers are possible and we're out of control for redirecting state
     * data about chosen auth provider, this data is transferred and saved as a browser secured cookie.
     * This cookie is temporal and automatically removed at the last step of authentication procedure.
     * It allows the chosen instance of this micro service knows which auth provider to use during
     * different redirects - Facebook, Google, Github or other.
     * !!!---Changing Grant Types is not yet implemented---!!!
     *
     * @param routingContext Vertx RoutingContext data, containing everything related to current
     *                       connection including request, response, etc.
     */
    private void authentication(RoutingContext routingContext, OAuth2GrantType oAuth2GrantType) {
        // Initially we need to check if there is a header exists with any kind of allowed auth providers.
        // It is necessary to let UI application (thus the customer) to choose which OAuth 2.0 provider
        // he/she would like to use for authentication purposes. IT IS REQUIRED DATA.
        // In case there will be no header with provider information, sent by UI application, then
        // configuration MUST have default providers, chosen from the list of supported providers.
        String requestAuthProvider = routingContext.request().getHeader(HttpConstants.OAUTH2_AUTH_PROVIDER);
        String selectProvider      = null;
        // some portion of validation for the data above and selecting proper provider by UI or predefined
        if (!Utils.isNullOrBlank(requestAuthProvider)) {
            selectProvider = requestAuthProvider;
        } else {
            selectProvider = configurator.getFromCustomConfig(HttpConstants.OAUTH2_DEFAULT_PROVIDER);
        }

        // this is the Facade for creating actual OAuth 2.0 provider and passing it back to work here
        this.authProvider = new OAuth2AuthProvider(configurator, routingContext, selectProvider);
        this.oauth2       = authProvider.selectProvider().create();

        // as well as prepare the string for auth request
        String authURI = oauth2.authorizeURL(this.authProvider.selectProvider()
            .getAuthUrl(routingContext, getHostUrl(routingContext)));

        // at the same time adding cookie to choose the SAME OAuth 2.0 provider to continue
        // Auth Code Flow to be finally resolved and do not choose wrong one
        Cookie OAuthProvider = Cookie.cookie(HttpConstants.OAUTH2_AUTH_PROVIDER, selectProvider)
            .setPath("/")
            .setMaxAge(60)
            .setSecure(true);
        routingContext.addCookie(OAuthProvider);

        // With the [Location] header returning status 302 initiate Http(-s) request to the OAuth 2.0 provider
        routingContext.response()
            .putHeader("Location", authURI)
            .setStatusCode(302)
            .end();
    }

    private void authCallback(RoutingContext routingContext) {
        String selectProvider = routingContext.request().getCookie(HttpConstants.OAUTH2_AUTH_PROVIDER).getValue();
        this.authProvider = new OAuth2AuthProvider(configurator, routingContext, selectProvider);
        this.oauth2       = authProvider.selectProvider().create();

        // code is a mandatory value
        String code = routingContext.request().getParam("code");
        if (code == null) {
            routingContext.fail(400);
            return;
        }

        String redirectTo = routingContext.request().getParam("redirect_uri");
        String redirectUri = getHostUrl(routingContext)
                             + routingContext.currentRoute().getPath()
                             + "?redirect_uri=" + redirectTo;

//        try {
//            redirectTo = URLEncoder.encode(redirectUri, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }

        JsonObject url = new JsonObject()
            .put("code", code)
            .put("redirect_uri", redirectUri);

        // shit happens and sometimes Netty v.4.x fails to resolve DNS records correctly from the first attempt
        // There is open bug, but they can't fix it somehow, promising to deliver changes in v.5.x
        // So we need to repeat up to 3 attempts, crossing fingers it will work :(
        //performAuth(routingContext, redirectTo, url, new AtomicInteger(0));
        performAuth(routingContext, redirectTo, url, new AtomicInteger(0));
    }


    /**
     * This method perform actual authentication procedure using Vert.X library.
     * Sometimes Netty v.4.x fails to resolve DNS records correctly from the first attempt
     * So we need to repeat up to 3 attempts, crossing fingers it will work :( and thus there recursive call for that
     *
     * @param routingContext Vertx RoutingContext data, containing everything related to current
     *                       connection including request, response, etc.
     * @param redirectTo     String containing where this call will be redirected after Auth Provider resolve it
     * @param url            JsonObject with data needs to be included as query parameters
     * @param atomicCounter  AtomicInteger is used to count attempts through recursive calls. It's Atomic type
     *                       since it works inside lambda
     */
    private void performAuth(RoutingContext routingContext, String redirectTo, JsonObject url, AtomicInteger atomicCounter) {
        oauth2.authenticate(url, asyncTokenResult -> {
            if (asyncTokenResult.failed()) {
                int counter = atomicCounter.incrementAndGet();
                log.warn("Auth attempt #{} failed. Next attempt...", counter);
                log.trace("Reason for failure:", asyncTokenResult.cause());
                if (counter == 3) {
                    routingContext.fail(asyncTokenResult.cause());
                } else {
                    performAuth(routingContext, redirectTo, url, atomicCounter);
                }
            } else {
                routingContext.response().putHeader("Location", redirectTo);
                log.info("User authenticated successfully");

                // finally when we get token from our Auth Provider, we want to get information about user
                this.authProvider.selectProvider().retrieveUserEmails(this, routingContext, asyncTokenResult.result());
            }
        });
    }

    public void finalizeAuth(RoutingContext routingContext, String email) {
        Cookie OAuthProvider = Cookie.cookie("token", createJWT(email))
            .setPath("/")
            .setMaxAge(60)
            .setSecure(true);
        routingContext.addCookie(OAuthProvider);
        routingContext.response().setStatusCode(302).end();
    }

    private String createJWT(String email) {
        String token = this.jwtAuthProvider.generateToken(new JsonObject().put("email", email),
            new JWTOptions().setAlgorithm("RS512"));
        log.trace(token);
        return token;
    }

    private JWTAuth createJwtAuthProvider() {
        // here we need to provide data stored in our PEM files but without first and last lines
        // so in your [bash_profile] or any other file just export 2 variables, make them system ones:
        // export publk=$(awk '/-END PUBLIC KEY-/ { p = 0 }; p; /-BEGIN PUBLIC KEY-/ { p = 1 }' <public_key>)
        // export privk=$(awk '/-END PRIVATE KEY-/ { p = 0 }; p; /-BEGIN PRIVATE KEY-/ { p = 1 }' <private_key_pk8>)
        PubSecKeyOptions pubSecKeyOptions = new PubSecKeyOptions();
        pubSecKeyOptions.setAlgorithm("RS512")
            .setPublicKey(System.getenv("publk"))
            .setSecretKey(System.getenv("privk"));

        JWTAuthOptions config = new JWTAuthOptions().addPubSecKey(pubSecKeyOptions);
        return JWTAuth.create(vertx, config);
    }

    private void authLogout(RoutingContext routingContext) {

    }

    /**
     * This is the actual method which assign handler to routes in Vert.X.
     * It is used many times by declaration code (see [Routes] object), to
     * register as many controller, as it is necessary for declareController.
     *
     * @param httpMethod     List of methods to use for registering controller
     * @param path           actual path which needs to be added to a router handler
     * @param callBack       method name from [controller] object to serve the response
     * @param defaultHeaders map containing headers for HTTP connection
     */
    private void controllerImpl(HttpMethod httpMethod, String path, String callBack,
                                Map<String, String> defaultHeaders) {

        router.route(httpMethod, path).handler(routingContext -> {
            // for initial authN/Z we use cookie to pass the token through OAuth2 redirects
            // but later we need to include the JWT token into each request sent by client
            // so initial cookie should be deleted after authentication
            Cookie cookieToken = routingContext.getCookie("token");
            String authHeader  = routingContext.request().getHeader(HttpConstants.HEADER_AUTHORIZATION);

            // if there is a cookie and it is not empty - which is happened during initial authentication
            if (cookieToken != null && !Utils.isNullOrBlank(cookieToken.getValue())) {
                routingContext.removeCookie("token");
                log.debug("Authorization cookie removed, validating JWT token");
                validateJWTToken(routingContext, callBack, cookieToken.getValue());
            }
            // otherwise if there is a header with JWT
            else if (!Utils.isNullOrBlank(authHeader)) {
                log.debug("Authorization header is set, validating JWT token");
                validateJWTToken(routingContext, callBack, authHeader);
            }
            // if nothing present - it means user is not authorized
            else {
                // get all URIs which require auth
                JsonArray authContent = configurator.getFromCustomConfig(HttpConstants.AUTHZ_PATHS);
                // if there's any match then this request require authorization
                if (authContent.stream().anyMatch(uri -> path.split(uri.toString()).length > 1)) {
                    log.debug("Current route [{}] requires authorization, but no JWT provided. Redirecting", path);
                    authentication(routingContext);
                }
                // even though user is able to reach out unprotected content
                else {
                    invokeEndpoint(routingContext, callBack);
                }
            }
        });
        // log what paths have been registered
        log.debug("Registered route: [{}, {}]", httpMethod, path);
    }

    private void validateJWTToken(RoutingContext routingContext, String callBack, String authHeader) {
        if (authHeader.startsWith("Bearer")) {
            authHeader = authHeader.substring(7);
        }
        this.jwtAuthProvider.authenticate(new JsonObject().put("jwt", authHeader), res -> {
            if (res.succeeded()) {
                log.debug("JWT token is valid");
                routingContext.setUser(res.result());
                //User theUser = res.result();
                invokeEndpoint(routingContext, callBack);
            } else {
                log.warn("JWT token is invalid. Redirecting");
                authentication(routingContext);
            }
        });
    }

    /**
     * Called by controllerImpl when user has been authorized to invoke the endpoint.
     * Add Authorization response header to optimize subsequent calls.
     *
     * @param routingContext routingContext of route
     * @param callBack       method name from [controller] object to serve the response
     */
    private void invokeEndpoint(RoutingContext routingContext, String callBack) {
        // Using reflection we have to identify a few things before we pass the control.
        // Please notice! After we catch the request and apply a logic in [Controller]
        // (i.e. [controller] object), the control will be return here! Just FYI
        // We need to identify the [controller] object of type [Controllers] and
        // method inside of that class passed here to serve that request.
        // After we do that, wi need to call that method dynamically.
        try {
            Method method = controller.getClass().getMethod(callBack, RoutingContext.class);
            method.invoke(controller, routingContext);
        } catch (InvocationTargetException e) {
            log.error("Error happened during call controller [{}]. Error Message: [{}] Stacktrace: ", callBack, e.getMessage(), e);
        } catch (NoSuchMethodException e) {
            log.error("Controller [{}] is not found. Stacktrace: {}", callBack, e);
        } catch (IllegalAccessException e) {
            log.error("Access violation calling controller [{}]. Stacktrace: {}", callBack, e);
        }
    }
}
