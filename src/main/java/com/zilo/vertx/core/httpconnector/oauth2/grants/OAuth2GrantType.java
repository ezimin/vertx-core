package com.zilo.vertx.core.httpconnector.oauth2.grants;

import java.util.EnumSet;

public enum OAuth2GrantType {
    AUTH_CODE,
    CLIENT,
    PASSWORD,
    AUTH_JWT;

    public static <E extends Enum<E>> boolean contains(String value) {
        try {
            return EnumSet.allOf(OAuth2GrantType.class).contains(Enum.valueOf(OAuth2GrantType.class, value));
        } catch (Exception e) {
            return false;
        }
    }
}
