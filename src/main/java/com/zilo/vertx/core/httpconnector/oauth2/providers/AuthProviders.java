package com.zilo.vertx.core.httpconnector.oauth2.providers;

import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.web.RoutingContext;

public interface AuthProviders {
    public OAuth2Auth create();
    public void retrieveUserEmails(HttpServer httpServer, RoutingContext routingContext, User user);
    public JsonObject getAuthUrl(RoutingContext routingContext, String fullUrl);
}
