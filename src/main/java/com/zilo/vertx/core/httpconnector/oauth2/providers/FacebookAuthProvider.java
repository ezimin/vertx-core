package com.zilo.vertx.core.httpconnector.oauth2.providers;

import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.constants.HttpConstants;
import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import com.zilo.vertx.core.launcher.Launcher;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.OAuth2ClientOptions;
import io.vertx.ext.auth.oauth2.OAuth2FlowType;
import io.vertx.ext.auth.oauth2.providers.FacebookAuth;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FacebookAuthProvider implements AuthProviders {

    Configurator customConfig;

    private static final Logger log = LogManager.getLogger(FacebookAuthProvider.class);

    public FacebookAuthProvider(Configurator configurator) {
        this.customConfig = configurator;
    }

    @Override
    public OAuth2Auth create() {
        return OAuth2Auth.create(Launcher.getVertx(), new OAuth2ClientOptions(new HttpClientOptions())
            .setSite("https://www.facebook.com")
            .setTokenPath("https://graph.facebook.com/oauth/access_token")
            .setAuthorizationPath("/dialog/oauth")
            .setUserInfoPath("https://graph.facebook.com/me")
            .setScopeSeparator(",")
            .setClientID("2528382343896371")
            .setClientSecret("d9ec93495d99d55da39dc61d190de10f"));
    }

    @Override
    public void retrieveUserEmails(HttpServer httpServer, RoutingContext routingContext, User user) {
        WebClient client = WebClient.create(Launcher.getVertx());
        HttpRequest<Buffer> request = client.getAbs("https://graph.facebook.com/me")
            .putHeader("Authorization", "token " + user.principal().getString("access_token"));

        request.send(response -> {
            if (response.succeeded()) {
                HttpResponse<Buffer> answer = response.result();
                String               result = answer.bodyAsString();

                JsonArray emails = new JsonArray(result);
                String email = emails.stream()
                    .filter(elem -> ((JsonObject) elem).getBoolean("primary"))
                    .map(em -> ((JsonObject) em).getString("email"))
                    .findAny().get();

                // an now get back to HttpServer
                httpServer.finalizeAuth(routingContext, email);
            } else {
                log.debug(response.cause().getMessage());
            }
        });
    }

    @Override
    public JsonObject getAuthUrl(RoutingContext routingContext, String fullUrl) {
        // Building all necessary links for OAuth 2.0 Auth Code Flow
        String from     = fullUrl;
        String to       = fullUrl + routingContext.request().path();
        String callback = this.customConfig.getFromCustomConfig(HttpConstants.OAUTH2_CALLBACK);

        return new JsonObject()
            .put("redirect_uri", from + callback + "?redirect_uri=" + to)
            .put("scope", "email")
            .put("state", "");
    }
}
