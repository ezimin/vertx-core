package com.zilo.vertx.core.httpconnector.oauth2.providers;

import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.constants.HttpConstants;
import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import com.zilo.vertx.core.launcher.Launcher;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.providers.GithubAuth;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GitHubAuthProvider implements AuthProviders {

    Configurator customConfig;

    private static final Logger log = LogManager.getLogger(GitHubAuthProvider.class);

    public GitHubAuthProvider(Configurator configurator) {
        this.customConfig = configurator;
    }

    @Override
    public OAuth2Auth create() {
        return GithubAuth.create(Launcher.getVertx(),
            customConfig.getFromCustomConfig(HttpConstants.OAUTH2_CLIENT_ID).toString(),
            customConfig.getFromCustomConfig(HttpConstants.OAUTH2_CLIENT_SECRET).toString());
    }

    @Override
    public void retrieveUserEmails(HttpServer httpServer, RoutingContext routingContext, User user) {
        WebClient client = WebClient.create(Launcher.getVertx());
        HttpRequest<Buffer> request = client.getAbs("https://api.github.com/user/emails")
            .putHeader("Authorization", "token " + user.principal().getString("access_token"));

        request.send(response -> {
            if (response.succeeded()) {
                HttpResponse<Buffer> answer = response.result();
                String               result = answer.bodyAsString();

                JsonArray emails = new JsonArray(result);
                log.debug("Information about user from GitHub retrieved successfully");
                String email = emails.stream()
                    .filter(elem -> ((JsonObject) elem).getBoolean("primary"))
                    .map(em -> ((JsonObject) em).getString("email"))
                    .findAny().get();

                // an now get back to HttpServer
                httpServer.finalizeAuth(routingContext, email);
            } else {
                log.warn("There something wrong during user info retrieval: {}", response.cause().getMessage());
            }
        });
    }

    @Override
    public JsonObject getAuthUrl(RoutingContext routingContext, String fullUrl) {
        // Building all necessary links for OAuth 2.0 Auth Code Flow
        String from     = fullUrl;
        String to       = fullUrl + routingContext.request().path();
        String callback = this.customConfig.getFromCustomConfig(HttpConstants.OAUTH2_CALLBACK);

        return new JsonObject()
            .put("redirect_uri", from + callback + "?redirect_uri=" + to)
            .put("scope", "user:email")
            .put("state", "");
    }
}
