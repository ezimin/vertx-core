package com.zilo.vertx.core.httpconnector.oauth2.providers;

import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.constants.HttpConstants;
import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import com.zilo.vertx.core.launcher.Launcher;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.providers.GoogleAuth;
import io.vertx.ext.web.RoutingContext;

public class GoogleAuthProvider implements AuthProviders {

    Configurator customConfig;

    public GoogleAuthProvider(Configurator configurator) {
        this.customConfig = configurator;
    }

    @Override
    public OAuth2Auth create() {
        return GoogleAuth.create(Launcher.getVertx(),
            customConfig.getFromCustomConfig(HttpConstants.OAUTH2_CLIENT_ID).toString(),
            customConfig.getFromCustomConfig(HttpConstants.OAUTH2_CLIENT_SECRET).toString());
    }

    @Override
    public void retrieveUserEmails(HttpServer httpServer, RoutingContext routingContext, User user) {

    }

    @Override
    public JsonObject getAuthUrl(RoutingContext routingContext, String fullUrl) {
        return null;
    }
}
