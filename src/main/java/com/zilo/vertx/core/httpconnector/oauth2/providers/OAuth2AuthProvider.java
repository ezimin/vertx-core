package com.zilo.vertx.core.httpconnector.oauth2.providers;

import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.constants.HttpConstants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.Cookie;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OAuth2AuthProvider extends AbstractVerticle {

    private AuthProviders  authProvider;
    private Configurator   configurator;
    private RoutingContext routingContext;
    private String         authProviderSelector;

    private static final Logger log = LogManager.getLogger(OAuth2AuthProvider.class);

    public OAuth2AuthProvider(RoutingContext routingContext) {
        this.configurator         = setConfigurator();
        this.routingContext       = routingContext;
        this.authProviderSelector = setAuthProvider();
    }

    public OAuth2AuthProvider(Configurator configurator, RoutingContext routingContext) {
        this.configurator         = configurator;
        this.routingContext       = routingContext;
        this.authProviderSelector = setAuthProvider();
    }

    public OAuth2AuthProvider(Configurator configurator, RoutingContext routingContext, String authProviderSelector) {
        this.configurator         = configurator;
        this.routingContext       = routingContext;
        this.authProviderSelector = authProviderSelector;
    }

    public AuthProviders selectProvider() {
        return this.selectProvider(this.configurator, this.authProviderSelector);
    }

    public AuthProviders selectProvider(Configurator configurator) {
        return this.selectProvider(configurator, this.authProviderSelector);
    }

    public AuthProviders selectProvider(String authProviderSelector) {
        return this.selectProvider(this.configurator, authProviderSelector);
    }

    public AuthProviders selectProvider(Configurator customConfig, String chosenAuthProvider) {
        if (this.authProvider != null) {
            return this.authProvider;
        }
        JsonArray supportedProviders = ((JsonArray) (customConfig.getFromCustomConfig(HttpConstants.OAUTH2_SUPPORTED_PROVIDERS)));
        if (!supportedProviders.contains(chosenAuthProvider)) {
            this.routingContext.response().end(HttpConstants.HTTP401);
            throw new SecurityException("Cannot select proper OAuth2 provider. Auth provider is not allowed.");
        }

        switch (chosenAuthProvider) {
            case "github":
                this.authProvider = new GitHubAuthProvider(customConfig);
                break;
            case "google":
                this.authProvider = new GoogleAuthProvider(customConfig);
                break;
            case "facebook":
                this.authProvider = new FacebookAuthProvider(customConfig);
                break;
            default:
                this.routingContext.response().end(HttpConstants.HTTP401);
                throw new SecurityException("Cannot select proper OAuth2 provider. Auth provider is not allowed.");
        }
        return this.authProvider;
    }

    public JsonObject retrieveUserInfo() {
        return null;
    }

    private Configurator setConfigurator() {
        JsonObject configuration = ((JsonObject) Configurator.get(HttpConstants.SERVER)).copy();
        return new Configurator(configuration);
    }

    private String setAuthProvider() {
        String requestAuthProvider = this.routingContext.request().getHeader(HttpConstants.OAUTH2_AUTH_PROVIDER);
        Cookie cookieAuthProvider  = this.routingContext.getCookie(HttpConstants.OAUTH2_AUTH_PROVIDER);

        // in this case we getting data from header. Ususally this is then initial request from the UI
        // we need to set a cookie at the browser side to keep the service stateless
        if (requestAuthProvider != null && !requestAuthProvider.isBlank()) {
            Cookie authProvider = Cookie.cookie(HttpConstants.OAUTH2_AUTH_PROVIDER, requestAuthProvider)
                .setPath("/")
                .setMaxAge(60)
                .setSecure(true);
            this.routingContext.addCookie(authProvider);
            return requestAuthProvider;
        } else if (cookieAuthProvider != null) {
            this.routingContext.removeCookie(HttpConstants.OAUTH2_AUTH_PROVIDER);
            return this.routingContext.getCookie(HttpConstants.OAUTH2_AUTH_PROVIDER).getValue();
        } else {
            log.error("Cannot select proper OAuth2 provider. Auth provider neither set up in headers nor in cookies.");
            routingContext.response().end(HttpConstants.HTTP401);
            throw new SecurityException("Cannot select proper OAuth2 provider. Auth provider neither set up in headers nor in cookies.");
        }
    }
}
