package com.zilo.vertx.core.launcher;

import com.zilo.vertx.core.annotations.AutoInject;
import com.zilo.vertx.core.annotations.EnableHttpConnectors;
import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.dto.ComponentInstance;
import com.zilo.vertx.core.dto.ComponentType;
import com.zilo.vertx.core.httpconnector.impl.HttpClient;
import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("PlaceholderCountMatchesArgumentCount")
public class ComponentBuilder {
    private static final Logger log = LogManager.getLogger(ComponentBuilder.class);

    private static final String INITIAL_HTTP_SERVER    = "INITIAL_HTTP_SERVER";
    private static final String INITIAL_KAFKA_LISTENER = "INITIAL_KAFKA_LISTENER";
    private static final String INITIAL_COUCHBASE      = "INITIAL_COUCHBASE";
    private static final String INITIAL_CASSANDRA      = "INITIAL_CASSANDRA";
    private static final String APP_LOADING_TYPE       = "Application";
    private static final String COMP_LOADING_TYPE      = "Component";

    private Vertx  componentVertx;

    public ComponentBuilder() {
        this.componentVertx = Launcher.getVertx();
    }

    public ComponentBuilder(Vertx vertx) {
        this.componentVertx = Optional.ofNullable(vertx).orElse(Launcher.getVertx());
    }

    public List<String> prepareAnnotatedComponents(Class<?> mainClass) {
        this.initAnnotatedComponents(mainClass);
        ComponentLoader componentLoader = new ComponentLoader(APP_LOADING_TYPE);
        componentLoader.run(new ArrayList<>(Launcher.getComponents().keySet()));
        return new ArrayList<>(Launcher.getComponents().keySet());
    }

    public List<String> buildComponent(ComponentType componentType,
                                       String componentId,
                                       JsonObject componentConfiguration,
                                       Class<?> controllerClass) {

        return this.buildComponent(componentType,
            componentId,
            componentConfiguration,
            controllerClass,
            this.componentVertx);
    }

    private List<String> buildComponent(ComponentType componentType,
                                        String componentId,
                                        JsonObject componentConfiguration,
                                        Class<?> controllerClass,
                                        Vertx vertx) {

        JsonObject configuration = new JsonObject();
        if (componentConfiguration != null && !componentConfiguration.isEmpty()) {
            configuration = componentConfiguration;
        }
        ComponentInstance componentInstance = new ComponentInstance();
        componentInstance.setId(componentId);
        componentInstance.setComponentType(componentType);
        componentInstance.setConfiguration(configuration);
        componentInstance.setControllerClass(controllerClass);
        componentInstance.setVertx(vertx);
        componentInstance.setComponentReady(false);

        try {
            Method method = this.getClass().getDeclaredMethod(componentType.getInitMethod(), ComponentInstance.class);
            method.invoke(this, componentInstance);
        } catch (InvocationTargetException e) {
            log.error("Error happened during injecting dependencies. Stacktrace: {}", e);
        } catch (NoSuchMethodException e) {
            log.error("Controller is not found during dependencies injection. Stacktrace: {}", e);
        } catch (IllegalAccessException e) {
            log.error("Access violation calling during dependencies injection. Stacktrace: {}", e);
        }

        List<String> resultIds = new ArrayList<>();
        resultIds.add(componentId);
        resultIds.addAll(componentInstance.getDependants());

        return resultIds;
    }

    private void initAnnotatedComponents(Class<?> mainClass) {
        Annotation[] annotations = mainClass.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation instanceof EnableHttpConnectors) {
                List<String> resultIds = this.buildComponent(ComponentType.HTTP_SERVER, INITIAL_HTTP_SERVER,
                    Configurator.get(ComponentType.HTTP_SERVER.getDefaultConfig()),
                    ((EnableHttpConnectors) annotation).HttpController(),
                    this.componentVertx);

                log.info("There will be built following components: {}", resultIds);
            }
        }
    }

    public void initHttpServer(ComponentInstance componentInstance) throws ExceptionInInitializerError {
        log.debug("Creating and injecting HttpServer...");

        try {
            HttpServer httpServer = new HttpServer(componentInstance);
            componentInstance.setComponent(httpServer);
            Launcher.setComponent(componentInstance);
            List<ComponentType> dependencies = scanForControllerDependencies(componentInstance);
            dependencies.forEach(type -> {
                String id = componentInstance.getId() + "_" + type.toString();
                this.buildComponent(type, id, Configurator.get(type.getDefaultConfig()), null, this.componentVertx);
                componentInstance.getDependants().add(id);
            });

        } catch (Exception e) {
            log.error("Error happened during injecting HTTP Server Component. Stacktrace: {}", e);
        }
    }

    public void initHttpClient(ComponentInstance componentInstance) throws ExceptionInInitializerError {
        log.debug("Creating and injecting HttpClient...");
        try {
            HttpClient httpClient = new HttpClient(componentInstance);
            componentInstance.setComponent(httpClient);
            Launcher.setComponent(componentInstance);
        } catch (Exception e) {
            log.error("Error happened during injecting HTTP Client Component. Stacktrace: {}", e);
        }
    }

    private List<ComponentType> scanForControllerDependencies(ComponentInstance componentInstance) {
        List<ComponentType> types = new ArrayList<>();
        if (componentInstance.getControllerClass() != null) {
            Field[] fields = componentInstance.getControllerClass().getDeclaredFields();
            for (Field field : fields) {
                Annotation[] annotations = field.getDeclaredAnnotations();
                for (Annotation annotation : annotations) {
                    if (annotation instanceof AutoInject) {
                        String        label = field.getType().getSimpleName();
                        ComponentType type  = ComponentType.valueOfLabel(label);
                        if (!type.equals(componentInstance.getComponentType())) { types.add(type); }
                    }
                }
            }
        }
        return types;
    }
}
