package com.zilo.vertx.core.launcher;

import com.zilo.vertx.core.annotations.AutoInject;
import com.zilo.vertx.core.annotations.Endpoint;
import com.zilo.vertx.core.annotations.TopicHandler;
import com.zilo.vertx.core.dto.ComponentInstance;
import com.zilo.vertx.core.dto.Components;
import com.zilo.vertx.core.dto.Controllers;
import com.zilo.vertx.core.utils.Pair;
import com.zilo.vertx.core.httpconnector.impl.HttpServer;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.http.HttpMethod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("PlaceholderCountMatchesArgumentCount")
class ComponentLoader {
    //some constants
    private static final String INITIAL_HTTP_SERVER    = "INITIAL_HTTP_SERVER";
    private static final String INITIAL_KAFKA_LISTENER = "INITIAL_KAFKA_LISTENER";
    private static final String INITIAL_COUCHBASE      = "INITIAL_COUCHBASE";
    private static final String INITIAL_CASSANDRA      = "INITIAL_CASSANDRA";
    private static final String APP_LOADING_TYPE       = "Application";
    private static final String COMP_LOADING_TYPE      = "Component";

    private static final Logger log = LogManager.getLogger(ComponentLoader.class);

    private String loadingType;

    public ComponentLoader(String loadingType) {
        if (loadingType.equals(APP_LOADING_TYPE)) {
            this.loadingType = APP_LOADING_TYPE;
        } else { this.loadingType = COMP_LOADING_TYPE; }
    }

    public void run(List<String> componentIds) {
        List<Future> componentFutures = new ArrayList<>();
        for (String id : componentIds) {
            ComponentInstance componentInstance = Launcher.getComponent(id);
            componentFutures.add(componentInstance.getComponent().init());
        }
        CompositeFuture.all(componentFutures).setHandler(asyncResult -> {
            if (asyncResult.succeeded()) {
                log.debug("Following Component(-s) have been successfully instantiated {}", componentIds);

                // this method will be only run once for the whole application
                // just for convenience matter if you need to push something there.
                this.startMainInitMethod();

                for (String id : componentIds) {
                    ComponentInstance componentInstance = Launcher.getComponent(id);
                    if (componentInstance.getController() != null) {
                        try {
                            injectControllersDependencies(componentInstance);
                            startController(componentInstance);
                        } catch (InvocationTargetException e) {
                            log.error("Error happened during injecting dependencies. Stacktrace: {}", e);
                        } catch (NoSuchMethodException e) {
                            log.error("Controller is not found during dependencies injection. Stacktrace: {}", e);
                        } catch (IllegalAccessException e) {
                            log.error("Access violation calling during dependencies injection. Stacktrace: {}", e);
                        }
                    }
                }
            } else {
                log.error("Some Promises have not been successfully resolved. Reason {}", asyncResult.cause());
                log.info("Shutting down with error status");
                System.exit(1);
            }
            log.info("{}(-s) has been started successfully", this.loadingType);
        });
    }

    /**
     * Will run once for the whole App.
     */
    private void startMainInitMethod() {
        // the control flow will be here all the time even if there are no controllers
        // set for the initialing component. Just for convenience.
        if (loadingType.equals(APP_LOADING_TYPE)) {
            try {
                Method method = Launcher.getMainClass().getMethod("init");
                method.invoke(Launcher.getMainClass());
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                log.debug("[init()] method for Main class has not been called");
            }
        }
    }

    private void injectControllersDependencies(ComponentInstance componentInstance) throws
        NoSuchMethodException,
        InvocationTargetException,
        IllegalAccessException {

        Controllers controller = componentInstance.getController();

        List<Pair> setters = new ArrayList<>();
        Arrays.stream(controller.getClass().getDeclaredFields())
            .forEach(field -> setters.addAll(Arrays.stream(field.getDeclaredAnnotations())
                .filter(annotation -> annotation instanceof AutoInject)
                .map(instance -> new Pair<>("set"
                                            + Character.toUpperCase(field.getName().charAt(0))
                                            + field.getName().substring(1), field.getType()))
                .collect(Collectors.toCollection(ArrayList::new))));

        List<String>     dependantIds       = componentInstance.getDependants();
        List<Components> componentsToInject = new ArrayList<>();
        componentsToInject.add(componentInstance.getComponent());
        componentsToInject.addAll(dependantIds.stream()
            .map(instance -> {
                ComponentInstance el = Launcher.getComponent(instance);
                return el.getComponent();
            })
            .collect(Collectors.toCollection(ArrayList::new))
        );
        for (Pair setter : setters) {
            Method methodSetter = controller.getClass()
                .getMethod(setter.getLeft().toString(), (Class<?>) setter.getRight());
            for (Components component : componentsToInject) {
                if (component.getClass().equals(setter.getRight())) {
                    methodSetter.invoke(controller, component);
                }
            }
        }
    }

    private void startController(ComponentInstance componentInstance) throws IllegalAccessException, InvocationTargetException {
        Controllers controller = componentInstance.getController();
        try {
            // at first we try to invoke initializer if it exists
            Method method = controller.getClass().getMethod("init");
            method.invoke(controller);
        } catch (NoSuchMethodException e) {
            // nothing bad since that method is deprecated
            log.debug("[init()] method is not defined for controller: {}", controller.getClass().getSimpleName());
        }
        // finally, whenever [declareRoutes] exists or not, we scan Controller for
        // annotated methods and use them to register endpoints.
        Method[] methods = controller.getClass().getMethods();

        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                registerRoutesAndTopics(componentInstance, annotation, method);
            }
        }
    }

    private void registerRoutesAndTopics(ComponentInstance componentInstance, Annotation annotation, Method method) {

        if (annotation instanceof Endpoint) {
            // register routes for [HttpServer]
            HttpMethod requestMethod     = ((Endpoint) annotation).RequestMethod();
            String     uri               = ((Endpoint) annotation).uri();
            String     currentMethodName = method.getName();

            HttpServer httpServer = (HttpServer) componentInstance.getComponent();
            httpServer.endpoint(requestMethod, uri, currentMethodName);

        } else if (annotation instanceof TopicHandler) {
//            component = Optional.ofNullable(component).orElse(Launcher.getComponent(INITIAL_KAFKA_LISTENER));
//            KafkaListener kafkaListener = (KafkaListener) component;
//            kafkaListener.subscribeToTopic();
        }
    }
}
