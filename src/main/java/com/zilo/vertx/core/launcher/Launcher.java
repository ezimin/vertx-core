package com.zilo.vertx.core.launcher;

import com.zilo.vertx.core.configurator.Configurator;
import com.zilo.vertx.core.dto.ComponentInstance;
import com.zilo.vertx.core.dto.ComponentType;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Launcher {
    private static final Logger log = LogManager.getLogger(Launcher.class);

    @Setter
    @Getter
    private static Class<?> mainClass;

    @Getter
    @Setter
    private static boolean initialLoad = true;

    @Getter
    private static final Vertx vertx = Vertx.vertx();

    // components section
    @Getter
    private static final Map<String, ComponentInstance> components = new ConcurrentHashMap<>();

    private static final String COMPONENT = "Component";

    private Launcher() {
        throw new IllegalStateException("Utility Class");
    }
    public static <T> T getComponent(String key) {
        //noinspection unchecked
        return (T) components.get(key);
    }

    public static void setComponent(ComponentInstance component) {
        components.put(component.getId(), component);
    }

    public static boolean destroyComponent(String key) {
        if (components.containsKey(key)) {
            ComponentInstance component = components.get(key);
            vertx.undeploy(component.getDeploymentId(), result -> {
                components.remove(key);
                log.debug("Component {} undeployed and removed", key);
            });
            return true;
        }
        return false;
    }

    /**
     * This is the main method which initiates all the process.
     * It resolve the Configurator's promise and after that it checks for a flag,
     * responsible for enabling dynamic configuration. If that flag is enabled,
     * then it tries to load dynamic configuration from Kubernetes ConfigMaps and
     * Kubernetes Secrets. Otherwise - just jump to the next step.
     */
    public static void start(Class<?> mainClass) {
        setMainClass(mainClass);
        // we have to load configuration before anything started and pass it accordingly
        // so here is the object instantiation
        Configurator configurator = new Configurator();

        // actually loading configuration and passing it to other application components
        configurator.init().setHandler(asyncResponse -> {
            if (asyncResponse.succeeded()) {
                vertx.deployVerticle(configurator, response -> {
                    if (response.succeeded()) {
                        log.debug("Configurator verticle deployment ID is: {}", response.result());
                    }
                    Configurator.setConfiguration(asyncResponse.result());
                    configurator.loadDynamicConfiguration(mainClass);
                });
            }
        });
    }

    /**
     * Dynamic Loader responsible for initiating new components for the whole Factory
     *
     * @param componentType type of the new component
     * @param componentId   new component ID inside the Factory
     */
    public static void startComponent(ComponentType componentType,
                                      String componentId) {
        startComponent(componentType, componentId, new JsonObject(), null);
    }

    /**
     * Dynamic Loader responsible for initiating new components for the whole Factory
     *
     * @param componentType          type of the new component
     * @param componentId            new component ID inside the Factory
     * @param componentConfiguration configuration required for new Component to be initialized
     */
    public static void startComponent(ComponentType componentType,
                                      String componentId,
                                      JsonObject componentConfiguration) {
        startComponent(componentType, componentId, componentConfiguration, null);
    }

    /**
     * Dynamic Loader responsible for initiating new components for the whole Factory
     *
     * @param componentType          type of the new component
     * @param componentId            new component ID inside the Factory
     * @param componentConfiguration configuration required for new Component to be initialized
     * @param controllerClass        controller for new Component or [null]
     */
    @SuppressWarnings("WeakerAccess")
    public static void startComponent(ComponentType componentType,
                                      String componentId,
                                      JsonObject componentConfiguration,
                                      Class<?> controllerClass) {

        List<String> ids = new ComponentBuilder().buildComponent(componentType,
            componentId,
            componentConfiguration,
            controllerClass);
        new ComponentLoader(COMPONENT).run(ids);
    }

    public static List<String> createComponent(ComponentType componentType,
                                               String componentId,
                                               JsonObject componentConfiguration,
                                               Class<?> controllerClass) {

        return new ComponentBuilder().buildComponent(componentType,
            componentId,
            componentConfiguration,
            controllerClass);
    }

    public static void runComponent(String id) {
        ComponentInstance componentInstance = Launcher.getComponent(id);
        List<String>      ids               = new ArrayList<>(componentInstance.getDependants());
        ids.add(id);
        new ComponentLoader(COMPONENT).run(ids);
    }

    public static void runComponents(List<String> ids) {
        List<String> resolveIds = new ArrayList<>();
        for (String id : ids) {
            ComponentInstance componentInstance = Launcher.getComponent(id);
            resolveIds.addAll(new ArrayList<>(componentInstance.getDependants()));
            resolveIds.add(id);
        }
        new ComponentLoader(COMPONENT).run(resolveIds);
    }
}
