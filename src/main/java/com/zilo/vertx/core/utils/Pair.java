package com.zilo.vertx.core.utils;

import lombok.Data;

@Data
public class Pair<Left, Right> {

    private Left  left;
    private Right right;

    public Pair(Left left, Right right) {
        this.left  = left;
        this.right = right;
    }
}
