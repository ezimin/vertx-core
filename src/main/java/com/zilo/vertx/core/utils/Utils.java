package com.zilo.vertx.core.utils;

public class Utils {
    public static boolean isNullOrBlank(String string) {
        return string == null || string.isBlank();
    }
}
